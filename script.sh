#!/bin/bash

test=("Directory/subdir-a" "Directory/subdir-b" "Directory/subdir-c")

createDir() {
  for i in ${test[@]}; 
  do
    echo "mkdir -p $i"
    mkdir -p "$i"
  done
}

createFiles() {
  for i in ${test[@]}; 
  do
    echo "touch $i/file.txt"
    touch "$i/file.txt"
  done
}

editFilesAndEcho() {
  for i in ${test[@]}; 
  do
    echo "echo "Hello world!" >> $i/*"
    echo "Hello world!" >> $i/*
    echo "cat $i/*"
    cat $i/*
  done
}

deleteFiles() {
  for i in ${test[@]}; 
  do
    echo "rm -r $i/*"
    rm -r $i/*
  done
}

createDir
createFiles
editFilesAndEcho
deleteFiles
